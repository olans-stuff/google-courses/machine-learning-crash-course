# Machine learning Crash course
Parts of the course 
### [{Course Here} ]( https://developers.google.com/machine-learning/crash-course/framing/video-lecture )

* ML Concepts
	* Framing
	* Descending into ML
    * Reducing Loss
    * First Steps With TF
    * Generalisation
    * Training and Test Sets
    * Validation Set
    * Representation
    * Feature Crosses
    * Regularisation: Sparsity
    * Neural networks
    * Training Neural Nets
    * Multi-Class Neural Nets
    * Embeddings

#
* ML Engineering 
    * Production ML Systems
    * Static vs Dynamic Training
    * Static vs Dynamic Interference
    * Data Dependencies
    * Fairness
    #
* ML Systems in Real World
    * Cancer Prediction
    * Literature
    * Guidelines


# 

     
# 
# Framing: Key ML Terminology
What is (supervised) ML?
- Ml sys learn how to combine input to produce useful predictions on never-before-seen-data

***LABELS:*** Is the thing we are predicting-the `y` variable in simple linear regression. Could be the future price of wheat, kind of animla shown, meainging of audio, basically anything


 ***FEATURES:*** Input variable -the `x` variable in simple linear regression. A simple ml project might use single feature, more sophisticated ml project could use millions of features: x1,x2....xN

 In the spam detector example, features could include
 * words in email text
 * sender's address
 * time of day mail sent
 * email contains the phrase "one weird trick"


 ***EXAMPLES*** is a particular instance of data, **x** (We put **x** in bold to indicate its a vector). We break examples into:
 * labeled examples
 * unlabeled examples

 A ***labeled example*** includes both feature(s) and the label. That is:
 ~~~
   labeled examples: {features, label}: (x, y)
 ~~~

 Use labled examples to ***Train*** the model. In spam detector example, labeled examples would be individual emails that users have marked as "Spam" or "Not Spam"


 example, this shows 5 labeled exapmles about housing prices in California

| housingMedianAge(feature) | totalRooms (feature)| totalBedrooms (feature) |medianHouseValue(label)|
| --------------------------| --------------------|-------------------------|-----------------------|
| 15                        | 5612                |1283                     |66900                  |
|                           |                     |                         |                       |
| 19                        | 7650                |1901                     |80100                  |
|                           |                     |                         |                       |
| 17                        | 720                 |174                      |857000                 |
|                           |                     |                         |                       |
| 14                        | 1501                |337                      |73400                  |
|                           |                     |                         |                       |
| 20                        | 1454                |326                      |65500                  |
|                           |                     |                         |                       |

An ***unlabeled example*** contains features but not the label. That is: 
~~~
unlabeled examples: {features, ?}: (x, ?)
~~~

Here are 3 unlabeled examples from the same housing dataset, which exclude ```medianHouseValue```


| housingMedianAge(feature) | totalRooms (feature)| totalBedrooms (feature) |
| --------------------------| --------------------|-------------------------|
| 42                        | 1686                |361                      |
|                           |                     |                         |
| 34                        | 1226                |180                      |
|                           |                     |                         |
| 33                        | 1077                |271                      |

- Once trained model with labeled examples, we use model to predict label on unlabeled examples. In spam detector, unlabled are new emails that humans haven't labeled yet

***Models***
- A model defines relationship between features & label. Eg a spam detection model might associate certain features strongly with "spam". 

* Two phases of a model's life
   * ***Training*** means creating or ***learning*** the model. That is, you show the model labeled examples and enable the model to gradually learn the relationships between features and label
   * ***Inference*** means applying the trained model to unlabeled examples. That is, you use the trained model to make useful predictions ('y''). For example, during inference, uou can predict 'medianHouseValue for new unlabeled examples

   ### Regression vs classification

   A ***regression*** model predicts continuous values. Eg, they make predictions that answer questions like:
* What is value of house in California
* What is the probability that a user will click this ad?

A ***classification*** model predicts discrete values. For eg, classification models make predictions that answer questions like the following:
* Is a given email message spam or not spam?
* Is this an image of a dog, a cat, or a hamster

#
#  Descending into ML
## Video Lecutre
#### Learning from Data
* Lots complex ways learn from data
* Start simple to open door more sophisticated 
![graph](./Images/graphh.PNG)
- y - m,x, + b : w = slope, b = bias
- How know if good line
- Check for loss, look at prediction of x value to true value
![loss](./Images/loss.PNG)
- We care about minimising loss on entire data set
![loss2](./Images/loss2.PNG)

## Linear Regression
It's known crickets chirp more often on hooter days rather than cooler days. For decades, professional and amateur scientists have cataloged data on chirps-per-min & temperature. As a bday gift, ur Aunt Ruth gives u cricket database nd asks you to learn a model to predict relationship.
1. firs, you examine data by plotting  
![cricket](./Images/cricket.PNG)
`Fig1 represents chirps per mins vs temp in celsius`

Is this relationship Linear? Yes, you could draw a straight line through to approximate it
![cricketlinear](./Images/cricketlinear.PNG)
`Fig2 a linear relationship`

Line doesn't pass through every dot, but does show relationship between chirps and temperature. Use eq for line (ml way):


* ***y' = b + w1x1***
    * Where:
    * ***y'*** Is the predicted ![label](https://developers.google.com/machine-learning/crash-course/descending-into-ml/linear-regression) (a desired output).
    * ***w1***  Is the weight of feature 1. Weight is the same concept as the "slope" ***m*** 
 in the traditional equation of a line. 
    * ***x1***  is a ![feature](https://developers.google.com/machine-learning/crash-course/descending-into-ml/linear-regression) (a known input).


 To ***infer*** (predict) the temperature ***y'*** for a new chrips-per-min value ***x1***, just subsitute the ***x1*** value into this model.



 Although this model uses 1 feature, a more sophisticated model might rely on multiple features, each having a seperate weight (w1,w2,etc.). FOr eg, a model that relies on 3 features might look as follows:
`y' = b + w1x1 + w2x2 + w3x3`

## Training & Loss
***Training*** a model means learning good values for all the weights & the bias from labeled exampkes. In supervised learning, a ml algorithim builds a model by examining many examples and attempting to find a model that minimises loss;


This process is called ***empirical risk minimisation***


Loss is penatly for a bad prediction. ***Loss*** is a number indicating how bad the model's prediction was on a single example. If the model's prediction is perfect, the loss is zero; otherwise, the oss is greater. The goal of training a model is to find a set of weights and biases that have a *low* loss, average, across all examples. For eg, fig3 shows high loss model on the left and low loss model on the right

![idk](./Images/idk.PNG)
`Fig3 High loss in left model; low in right`
    